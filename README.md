# Remitano

Table of content

+ [Main Feautures](#main-feautures)
    + [Login](#login)
    + [Warning](#warning)
    + [Transaction](#transaction)
    + [Supported Cryto](#supported-cryto)
    + [Swap through USDT](#swap-through-usdt)
    + [Reference Reward](#reference-reward)

## Main Features

### Login

Login should be complex (^^), which verify user at least:

+ Identify Card (with real face)
+ Another document with user's name
+ Phone
+ Email

[Documents](images/login) must be submitted

### Warning

+ Dont talk `Bitcoin` when transfer through bank
+ Transaction must be send immediately

Screenshots:
![Warn when transfer through bank](images/warn-should-not-talk-bitcoin.png)
![Send immediately](images/send-immediately.png)

### Transaction

+ Block transaction with count down
+ Support Cancel & Refund

Screenshots:
![Block transaction  with countdown](images/transaction-count-down.png)
![Cancel Transaction & Support refund](images/cancel-transaction.png)

### Supported Cryto

+ ETH
+ USDT
+ BCH

![Supported Cryto](images/supported-crypto.png)

### Swap through USDT

+ Exchange through USDT

![Swap](images/swap.png)

### Reference Reward

+ Reward for reference

![Reward on reference](images/ref.png)